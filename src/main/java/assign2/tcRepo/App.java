package assign2.tcRepo;

import java.io.IOException;
import java.util.Map;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import assign2.basePageObject.BasePage;
import assign2.pageObjects.BookingPage;
import assign2.pageObjects.SearchResult;


public class App extends BasePage 
{	
	
	@Test(priority = 1)
	public void sce1() throws IOException, InterruptedException {
		
		System.out.println("Test : " +testDataFile);
		System.out.println("Test : " +node.get("jsonData"));

		@SuppressWarnings("unchecked")
		Map<String, String> travels = mapper.convertValue(node.get("jsonData"),Map.class); /* Store itinerary data into map */
		
		BookingPage page =new BookingPage(driver);
		page.clickOnTab(travels.get("booking"));
		Reporter.log("Click on Flight Tab");

		page.clickOnJourneyType(travels.get("journey"));
		Reporter.log("Click on Journey Type : "+ travels.get("journey"));
		
		page.selectFrom(travels.get("departure"));
		Reporter.log("Select journey start from  : "+ travels.get("departure"));

		page.selectTo(travels.get("arrival"));
		Reporter.log("Select journey end to  : "+ travels.get("arrival"));

		page.selectDate(travels.get("departuredate"), travels.get("tripwaydepart"));
		Reporter.log("Select journey depart date  : "+ travels.get("journey") );

		
		page.selectDate(travels.get("returnDate"), travels.get("tripwayreturning"));
		Reporter.log("Select journey return date  : "+ travels.get("tripwayreturning") );

		page.selectcount(travels.get("count"));
		Reporter.log("Select count for ticket : "+ travels.get("count") );

		SearchResult searchResultPage = page.clickSearchButton();
		
		expectedResult();
		
		searchResultPage.checkOnStopFilter(0);
		Assert.assertTrue(searchResultPage.verifyHeader(travels.get("arrival").toString()));
		Reporter.log("Verify label 'Select your departure to '");
		
		Assert.assertTrue(searchResultPage.verifyFlight());
		Reporter.log("Verify all flights related to search result");

	}
	
}
