package assign2.basePageObject;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;;

/**
 * This Page included all common utilities that require 
 * @author Dhaval.Modi
 *
 */

public class BasePage  {

	
	public static String projectDirectory = System.getProperty("user.dir");
	public static String testDataFile;
	
	public ObjectMapper mapper = new ObjectMapper();
	public JsonNode node;
	
	public WebDriver driver;
	

	/**
	 * This Method for Load pre execution process that require all configuration 
	 * 
	 * @param browser
	 * @param jsonfile
	 * @param url
	 */
	
	@Parameters("browser")
	@BeforeClass
	public void intializeBrowser(String browser)
	{
		loadBrowser(browser, "data.json", "https://www.orbitz.com/");
		driver = getDriver();
		node = getJsonNode();
	}
	
	public void loadBrowser(String browser , String jsonfile , String url ) {	
		switch (browser) {
		case "chrome":
			driver = loadChrome(url, jsonfile);
			break;
		case "firefox":
			
			driver = loadFirefox(url, jsonfile);
			break;
		case  "":
			break;
		}	
	}
	
	/**
	 * This method run after  test execution Completed close driver
	 * @throws InterruptedException
	 */
	@AfterTest
	public void closeDriver() throws InterruptedException {
		try{
		driver.quit();
		Thread.sleep(2000);
		}
		catch(IllegalMonitorStateException e)
		{
			e.printStackTrace();
		}
		
	}
	
	/**
	 * This Method is used for after all execution completed then open result for all test execution
	 * @throws InterruptedException
	 */
	@AfterSuite
	public void openFinalResult() throws InterruptedException {
	
		System.setProperty("webdriver.chrome.driver", projectDirectory + "\\driver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get(projectDirectory + "\\target\\surefire-reports\\index.html");
		driver.manage().window().maximize();
		driver.navigate().refresh();
	}

	/**
	 * This method return driver
	 * @return
	 */
	public WebDriver getDriver() {
		return this.driver;
	}

	/**
	 * This method is used to get data from json file
	 * @return
	 */
	public JsonNode getJsonNode() {

		try {
			FileInputStream jsonInputFile = new FileInputStream(testDataFile);
			node = mapper.readTree(jsonInputFile);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return node;
	}
	
	/**
	 * This Method is just log of expected result title
	 */
	public void expectedResult()
	{	Reporter.log("----------------------------------------------------------------");
		Reporter.log("Expected Result :");
	}
	
	/**
	 * This Method developed for click on particular element
	 * 
	 * @param by
	 */
	public void click(By by) {
		try {
			WebElement element = findElement(by);
			getHighlitedElement(element);
			element.click();
			timeout(1);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * This Method verify element is displayed or not
	 * 
	 * @param by
	 * @return
	 */
	public boolean isDisplayed(By by) {
		WebElement element = driver.findElement(by);
		getHighlitedElement(element);
		timeout(1);
		return element.isDisplayed();
	}

	/**
	 * This Method verify element is selected or not
	 * 
	 * @param by
	 * @return
	 */
	public boolean isSelected(By by) {
		WebElement element = findElement(by);
		getHighlitedElement(element);
		timeout(1);
		return element.isSelected();
	}

	/**
	 * This method select value from drop-down
	 * 
	 * @param by
	 * @param value
	 */
	public void selectFromDropdown(By by, String value) {
		Select selectByVisibleText = new Select(findElement(by));
		selectByVisibleText.selectByVisibleText(value);
		timeout(1);
	}

	/**
	 * This mMethod is implicit wait 
	 * 
	 * @param second
	 */
	public void timeout(int second) {
		driver.manage().timeouts().implicitlyWait(second, TimeUnit.SECONDS);

	}

	/**
	 * This method is used for send value in textfield
	 * 
	 * @param by
	 * @param value
	 */
	public void sendKeys(By by, String value) {
		WebElement element = findElement(by);
		element.sendKeys(value);
		timeout(1);
	}

	/**
	 * This method is used for get text from particular web-element 
	 * 
	 * @param by
	 * @return
	 */
	public String getText(By by) {
		WebElement element = findElement(by);
		getHighlitedElement(element);
		timeout(1);
		return element.getText();
	}

	/**
	 * This method is used for highlight element
	 * 
	 * @param element
	 * @return
	 */
	public WebElement getHighlitedElement(WebElement element) {
		try {
			JavascriptExecutor jsexe = (JavascriptExecutor) driver;
			jsexe.executeScript("arguments[0].setAttribute('style', 'border: 2px solid red;');", element);
		} catch (StaleElementReferenceException  e) {
			e.printStackTrace();
		}
		return element;

	}

	/**
	 * This method is used for scroll to element
	 * 
	 * @param by
	 */
	public void scrollToElement(By by) {
		WebElement element = findElement(by);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", element);

	}

	/**
	 * This method is used for get attribute value in element
	 * 
	 * @param by
	 * @param attributefield
	 * @return
	 */
	public String getAttribute(By by, String attributefield) {
		WebElement element = findElement(by);
		return element.getAttribute(attributefield);

	}

	/**
	 * This Method return list of elements that find 
	 * @param by
	 * @return
	 */
	public List<WebElement> findElements(By by) {
		List<WebElement> elements = driver.findElements(by);
		return elements;
	}

	/**
	 * This method find particular element
	 * 
	 * @param by
	 * @return
	 */
	public WebElement findElement(By by) {

		WebElement element = driver.findElement(by);
		return element;

	}

	/**
	 * 
	 * @return true if all ajax and js are loaded with in max time  limit
	 *         otherwise false
	 */
	public boolean isAjaxCallCompleted() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.ignoring(StaleElementReferenceException.class);
		try {
			return ((Boolean) wait.until(new ExpectedCondition<Boolean>() {
				public Boolean apply(WebDriver inDriver) {
					boolean z = isJQueryLoaded() && isJSLoaded();
					return Boolean.valueOf(z);
				}
			})).booleanValue();
		} catch (TimeoutException e) {
			return false;
		}
	}

	/**
	 * 
	 * @return true if all js are loaded with in max time limit  otherwise
	 *         false
	 */
	public boolean isJSLoaded() {
		return ((JavascriptExecutor) driver).executeScript("return document.readyState", new Object[0]).toString()
				.equals("complete");
	}

	/**
	 * 
	 * @return true if all jquery are loaded with in max time limit 
	 *         otherwise false
	 */
	public boolean isJQueryLoaded() {
		try {
			return ((Long) ((JavascriptExecutor) driver).executeScript("return jQuery.active", new Object[0]))
					.longValue() == 0;
		} catch (Exception e) {
			return true;
		}
	}

	/**
	 * 
	 * @param loggerName
	 * @return
	 */
	public Logger getLogger(String loggerName) {
		Logger logger = Logger.getLogger(loggerName.trim());
		return logger;
	}
	
	/**
	 * load chrome Driver and hit the url
	 * @param appURL
	 * @param jsonFile
	 * @return
	 */
	private WebDriver loadChrome(String appURL ,String jsonFile) {
		System.out.println("Launching chrome");
		System.setProperty("webdriver.chrome.driver", projectDirectory + "\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(appURL);
		testDataFile =  projectDirectory + "\\jsonReader\\"+jsonFile;
		driver.manage().window().maximize();
		node = getJsonNode();
		return driver;
	}
	
	/**
	 * load Firefox Driver and hit the url
	 * @param appURL
	 * @param jsonFile
	 * @return
	 */
	private WebDriver loadFirefox(String appURL ,String jsonFile) {
		System.out.println("Launching Firefox");
		System.setProperty("webdriver.gecko.driver", projectDirectory + "\\driver\\geckodriver.exe");
		driver = new FirefoxDriver();
		driver.get(appURL);
		testDataFile =  projectDirectory + "\\jsonReader\\"+jsonFile;
		driver.manage().window().maximize();
		return driver;
	}
	

}
