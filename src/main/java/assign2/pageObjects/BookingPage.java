package assign2.pageObjects;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;

import assign2.locator.pathlocator;

/**
 * This Class For Booking Page Related Methods
 * @author Dhaval.Modi
 *
 */


public class BookingPage extends pathlocator {

	@BeforeClass
	public void setDriver() {
		driver = getDriver();
	}

	public BookingPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}

	/**
	 * This method for click on tab Like flight,hotel etc
	 * 
	 * @param tab
	 */
	public void clickOnTab(String tab) {
		click(orbitz_FlighTab);
	}

	/**
	 * This method for click on journey type like one-way,return etc
	 * 
	 * @param journytype
	 */
	public void clickOnJourneyType(String journytype) {
		switch (journytype) {
		case "Roundtrip":
			click(orbitz_roundtrip);
			break;
		case "One way":
			click(orbitz_oneWay);
			break;
		case "Multi-city":
			click(orbitz_multicity);
			break;
		}
	}

	/**
	 * This method click on search button
	 * 
	 * @return
	 * @throws IOException
	 */
	public SearchResult clickSearchButton() throws IOException {

		click(orbitz_searchButton);
		timeout(2);
		if (isDisplayed(orbitz_resultpage)) {
			SearchResult searchResult = new SearchResult(driver);
			return searchResult;
		}
		return null;
	}

	/**
	 * This method for select source location
	 * 
	 * @param fromplace
	 */
	public void selectFrom(String fromplace) {

		findElement(orbitz_FlighFrom).clear();
		sendKeys(orbitz_FlighFrom, fromplace);
		timeout(2);
		By frompla = By.xpath(".//*[@id='typeaheadDataPlain']//li//b[text()='" + fromplace + "']");
		click(frompla);
	}

	/**
	 * This method select destination location
	 * 
	 * @param toplace
	 * @throws IOException
	 */
	public void selectTo(String toplace) throws IOException {

		sendKeys(orbitz_FlighTo, toplace);
		timeout(2);
		By toPlace = By.xpath(".//*[@id='typeaheadDataPlain']//li//b[text()='" + toplace + "']");
		click(toPlace);
	}

	/**
	 * This Method select journey data from datepicker
	 * 
	 * @param date
	 * @param tripway
	 * @throws IOException
	 */
	public void selectDate(String date, String tripway) throws IOException {
		String[] split = date.split("/");
		String month = split[0];
		String day = split[1];
		String year = split[2];

		String[] monthMMM = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

		String month1 = month.replaceFirst("^0*", "");
		String day1 = day.replaceFirst("^0*", "");

		int m = Integer.parseInt(month1);

		String monthn = monthMMM[m - 1];

		String n = monthn + " " + year;

		if ("Departing".equalsIgnoreCase(tripway))
			click(orbitz_datePickerForDepating);
		else if ("Returning".equalsIgnoreCase(tripway))
			click(orbitz_datePickerForReturning);

		while (!(getText(By.xpath(".//*[@class='datepicker-cal-month'][1]//caption")).equalsIgnoreCase(n)
				|| getText(By.xpath(".//*[@class='datepicker-cal-month'][2]//caption")).equalsIgnoreCase(n))) {
			click(orbitz_nextButton);
		}
		timeout(1);
		click(By.xpath(".//button[@data-year='" + year + "' and @data-month='" + (m - 1) + "' and @data-day='" + day1
				+ "' ]"));
	}

	/**
	 * This method used to select count for journey
	 * 
	 * @param count
	 */
	public void selectcount(String count) {
		selectFromDropdown(orbitz_AdultSelect, count);
	}

}
