package assign2.pageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;

import assign2.locator.pathlocator;

/**
 * This Class Contains Search result page related methods
 * @author Dhaval.Modi
 *
 */
public class SearchResult extends pathlocator{
	
	
	@BeforeClass
	public void setDriver() {
		driver = getDriver();
	}


	
	public SearchResult(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}
	
	
	
	String parentW;
	String childW;

	
	/**
	 * This Method verify flight location on search result page
	 * 
	 * @return
	 * @throws InterruptedException
	 * @throws StaleElementReferenceException
	 */
	public boolean verifyFlight() throws InterruptedException,StaleElementReferenceException
	{
		do
		{
			
		}while(!isAjaxCallCompleted());
		
		if(driver.getWindowHandles().size()>0)
		{
			switchToChildWindow();
			closeChildWindow();
			switchToRootWindow();
		}
	
		String departng = getAttribute(orbitz_departureAirpot, "value");
		String[] depart = departng.split("\\(");
		String shortdepart = depart[1].replace(")", "").trim();
		
		String arrival = getAttribute(orbitz_arrivalAirpot, "value");
		String[] arriv = arrival.split("\\(");
		String shortarriaval = arriv[1].replace(")", "").trim();
		
		timeout(3);
			
		List<WebElement>  elements  = findElements(orbitz_flightInfoList);
		int size = elements.size();
		int count = 0;
		for(int i=1;i<=size;i++)
		{
			scrollToElement(By.xpath(".//*[@id='flightModuleList']//following::div[@data-test-id='flight-info']["+i+"]"));
			String text  = getText(By.xpath(".//*[@id='flightModuleList']//following::div[@data-test-id='flight-info']["+i+"]"));
			String departfrom = "Departure airport:\n"+	shortdepart;
			String arrivalTo = "Arrival airport:\n"+	shortarriaval;	
			if((text.contains(departfrom)) && (text.contains(arrivalTo)))
				count++;
		}
		
	return (count==size);

			
	
	}
	
	/**
	 * This method verifies the header of search result.
	 * 
	 * @param destination
	 * @return
	 * @throws InterruptedException
	 * @throws StaleElementReferenceException
	 */
	public boolean verifyHeader(String destination) throws InterruptedException,StaleElementReferenceException
	{
		String header = getText(orbitz_searchResultHeader).trim();
		String header1 = "Select your departure to "+destination;
		return header1.equalsIgnoreCase(header);
	}
	
	/**
	 * This method is used for filter by stop count
	 * 
	 * @param stop
	 */
	public void checkOnStopFilter(int stop)
	{
		switch (stop) {
		case 0:
			click(orbitz_filterwith0stop);
			break;
		case 1:
			click(orbitz_filterwith1stop);
			break;
		case 2:
			click(orbitz_filterwith2stop);
			break;
		}
	}
	/**
	 * This Method Switch to Child window
	 */
	public void switchToChildWindow() {
		parentW = driver.getWindowHandle();
		for (String childWindow : driver.getWindowHandles()) {
			driver.switchTo().window(childWindow);
			childW = childWindow;
		}
	}
	/**
	 * This Method close to Child window
	 */
	public void closeChildWindow() {
		driver.switchTo().window(childW).close();
	}
	/**
	 * This Method Switch to root window
	 */
	public void switchToRootWindow() {
		driver.switchTo().window(parentW);
	}
}
