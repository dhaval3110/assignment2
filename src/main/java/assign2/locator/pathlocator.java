package assign2.locator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import assign2.basePageObject.BasePage;

/**
 * This Class For only path locator
 * @author Dhaval.Modi
 *
 */
public class pathlocator extends BasePage {
	
	public pathlocator() {
		// TODO Auto-generated constructor stub
		super();
	}
	

	protected By orbitz_FlighTab = By.id("tab-flight-tab-hp");
	protected By orbitz_FlighFrom = By.id("flight-origin-hp-flight");
	protected By orbitz_FlighTo = By.id("flight-destination-hp-flight");
	protected By orbitz_AdultSelect = By.id("flight-adults-hp-flight");
	protected By orbitz_DatePickerFromdeparting = By.id("flight-departing-hp-flight");
	protected By orbitz_oneWay = By.id("flight-type-one-way-label-hp-flight");
	protected By orbitz_datePickerForDepating = By.id("flight-departing-hp-flight");
	protected By orbitz_datePickerForReturning = By.id("flight-returning-hp-flight");
	protected By orbitz_searchButton = By.xpath(".//*[@id='section-flight-tab-hp']//button[@type='submit']");
	protected By orbitz_nextButton = By.xpath(".//*[@class='datepicker-paging datepicker-next btn-paging btn-secondary next']");
	protected By orbitz_roundtrip = By.id("flight-type-roundtrip-label-hp-flight");
	protected By orbitz_oneway = By.id("flight-type-one-way-label-hp-flight");
	protected By orbitz_multicity = By.id("flight-type-multi-dest-label-hp-flight");
	protected By orbitz_resultpage = By.id("flightSearchResultDiv");
	
	protected By orbitz_departureAirpot = By.id("departure-airport-1");
	protected By orbitz_arrivalAirpot = By.id("arrival-airport-1");
	protected By orbitz_flightInfoList = By.xpath(".//*[@id='flightModuleList']//following::div[@data-test-id='flight-info']");
	protected By orbitz_searchResultHeader = By.xpath(".//*[@id='titleBar']//following::span[@class='title-city-text']");
	protected By orbitz_filterwith0stop = By.id("stopFilter_stops-0");
	protected By orbitz_filterwith1stop = By.id("stopFilter_stops-1");
	protected By orbitz_filterwith2stop = By.id("stopFilter_stops-2");
	
	
	
	
	
	
	

}
