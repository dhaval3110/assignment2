package listeners;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;

import assign2.basePageObject.BasePage;



public class Testlistener  extends BasePage implements ITestListener   {

	@BeforeClass
	public void setDriver() {
		driver = getDriver();
	}

	public Testlistener() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	
	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	
	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub
		System.out.println("***** Pass "+result.getName()+" test has Passed *****");

	}

	
	public void onTestFailure(ITestResult result) {
		// TODO Auto-generated method stub
		
		TakesScreenshot takescreen = (TakesScreenshot) driver;
        File scrFile =takescreen.getScreenshotAs(OutputType.FILE);
        String packageName = result.getInstanceName().substring(0, result.getInstanceName().lastIndexOf(46));
        String testName = result.getInstanceName().substring(result.getInstanceName().lastIndexOf(46) + 1);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
        String todaysDate = dtf.format(LocalDate.now());
        File screenShotFile = new File(BasePage.projectDirectory + "//screenshot//" + packageName + "//" + todaysDate + "//" + testName+"_" + LocalTime.now().toString().replaceAll(":", "_") + ".jpg");
        try {
				FileUtils.copyFile(scrFile, screenShotFile);
				logscreen(screenShotFile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
           
	}
	
	  protected void logscreen(File file) {
	        System.setProperty("org.uncommons.reportng.escape-output", "false");
	        String absolute = file.getAbsolutePath();
	        int beginIndex = absolute.indexOf(".");
	        String relative = absolute.substring(beginIndex).replace(".\\","");
	        String screenShot = relative.replace('\\','/');
	    
	    
	        Reporter.log("<a href=\"" + screenShot + "\"><p align=\"left\">Error screenshot at " + new Date()+ "</p>");
	        Reporter.log("<p><img width=\"1024\" src=\"" + file.getAbsoluteFile()  + "\" alt=\"screenshot at " + new Date()+ "\"/></p></a><br />"); 
	  }

	
	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
		
	}
	
	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
		
	}

	
	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
		
	}


}
